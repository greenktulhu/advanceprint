def print_tab(*args, tab_width=5):
    def strip_if_single(i):
        if len(i) == 1: return i[0], True
        else:           return i, False

    def str_and_spaces(i, tab_width):
        i = str(i)
        ret = i + " " * (tab_width - len(i))
        return ret
    
    args, s = strip_if_single(args)
    pr = ''
    # print(f"input:{args} type:{type(args)} single:{s}")
    
    if s:
        pr = str_and_spaces(args, tab_width)
    else:
        for a in args:
            pr += str_and_spaces(a, tab_width)

    print(pr)


for i in range(6):
    print_tab(i, i*i, i**i)
